# Sphere panorama viewer 3D

A program to view 3d panorama pictures.

Made By Hans Zuidervaart.

## Screenshots

#### screenshot 1: A preview of the application
![3DImageViewer](SmallWorldViewer2.png)

#### screenshot 2: 
![3DImageViewer](SmallWorldViewer1.png)

## Motivation
To view my drone 3d sphere panorama's 

## Sources
