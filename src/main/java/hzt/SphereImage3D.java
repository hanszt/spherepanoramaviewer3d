package hzt;

import hzt.service.FileChooserService;
import hzt.service.MouseControlService;
import hzt.utils.FxSetup;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.*;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Sphere;
import javafx.scene.transform.Rotate;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.*;

/**
 * @author afsal villan
 * @version 1.0
 * <p>
 * http://www.genuinecoder.com
 */
public class SphereImage3D extends Application {

    private static final Logger LOGGER = LogManager.getLogger(SphereImage3D.class);
    private static final float WIDTH = 1400;
    private static final float HEIGHT = 800;
    private static final int MIN_TRANSLATE_Z = -750;

    private final Map<String, String> pictureNameToFileNameMap = new HashMap<>();
    private final ComboBox<String> comboBox = new ComboBox<>();

    @Override
    public void start(Stage primaryStage) {
        Sphere sphere = prepare3DImageViewSphere();

        DoubleProperty deltaRotation = new SimpleDoubleProperty();
        prepareRotationRoundYAxis(sphere, deltaRotation);
        Group world = new Group(sphere);

        fillPictureCombobox(RESOURCES_DIR + "input");
        comboBox.setOnAction(e -> setPictureOnSphere(sphere));
        setPictureOnSphere(sphere);

        final VBox controlVbox = buildControlsVBox(primaryStage, sphere, deltaRotation);

        Group subSceneRoot = new Group(world);
        SubScene subScene = new SubScene(subSceneRoot, WIDTH, HEIGHT, false, SceneAntialiasing.BALANCED);
        subScene.setCamera(preparedPerspectiveCamera());

        Scene scene = getScene(controlVbox, subScene);
        subScene.widthProperty().bind(scene.widthProperty());
        subScene.heightProperty().bind(scene.heightProperty());

        scene.setOnKeyTyped(e -> recenterWhenRTyped(world, e));
        new MouseControlService(world).initMouseControl(scene);
        world.translateZProperty().addListener((o, c, n) -> limitTranslateZ(world, n));
        FxSetup.configureStage(primaryStage, scene, "3D panorama viewer").show();
        primaryStage.addEventFilter(KeyEvent.KEY_TYPED, key -> switchFullScreenIfFTyped(primaryStage, key));
    }

    private VBox buildControlsVBox(Stage primaryStage, Sphere sphere, DoubleProperty deltaRotation) {
        final Button chooseFileButton = configuredFileChooserButton(primaryStage, sphere);
        final Button resetRotationButton = FxSetup.configuredButton("Reset rotation", e -> deltaRotation.set(0));
        final Slider rotationSpeedSlider = new Slider(-.5, .5, 0);
        deltaRotation.bindBidirectional(rotationSpeedSlider.valueProperty());

        final VBox controlVbox = FxSetup.configuredVBox(comboBox, chooseFileButton, resetRotationButton, rotationSpeedSlider);
        controlVbox.toFront();
        controlVbox.setStyle("-fx-base: rgb(40, 40, 40);-fx-background: rgb(60, 60, 60);");
        return controlVbox;
    }

    private void limitTranslateZ(Group world, Number t1) {
        if (t1.doubleValue() < MIN_TRANSLATE_Z) world.setTranslateZ(MIN_TRANSLATE_Z);
    }

    private void recenterWhenRTyped(Group world, KeyEvent e) {
        if (e.getCharacter().equals("r")) {
            world.setTranslateX(0);
            world.setTranslateY(0);
        }
    }

    private void switchFullScreenIfFTyped(Stage stage, KeyEvent key) {
        if (key.getCharacter().equals("f")) {
            stage.setFullScreen(!stage.isFullScreen());
        }
    }

    private Scene getScene(Node... nodes) {
        AnchorPane root = new AnchorPane(nodes);
        root.setBackground(background(new Image(getClass().getResourceAsStream(RESOURCES_DIR + "galaxy.jpg")), root));
        return new Scene(root, WIDTH, HEIGHT);
    }

    private Background background(Image image, AnchorPane animationPane) {
        return new Background(new BackgroundImage(image,
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.CENTER,
                new BackgroundSize(animationPane.getWidth(), animationPane.getHeight(),
                        false, false, false, true)));
    }


    private Button configuredFileChooserButton(Stage primaryStage, Sphere sphere) {
        FileChooser fileChooser = new FileChooser();
        FileChooserService.configureFileChooser(fileChooser);
        final Button chooseFileButton = new Button("Choose picture file");
        chooseFileButton.setOnAction(e -> choosePictureFileOnSphere(fileChooser, primaryStage, sphere));
        return chooseFileButton;
    }

    private Camera preparedPerspectiveCamera() {
        PerspectiveCamera camera = new PerspectiveCamera(true);
        camera.setNearClip(.1);
        camera.setFarClip(10_000);
        camera.setTranslateZ(-1000);
        camera.setFieldOfView(30);
        return camera;
    }

    private void prepareRotationRoundYAxis(Node node, DoubleProperty deltaRotation) {
        node.setRotationAxis(Rotate.Y_AXIS);
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                node.setRotate(node.getRotate() + deltaRotation.get());
            }
        };
        timer.start();
    }

    private void choosePictureFileOnSphere(FileChooser fileChooser, Stage stage, Sphere sphere) {
        final File selectedFile = fileChooser.showOpenDialog(stage);
        if (selectedFile != null) {
            String absolutePath = selectedFile.getAbsolutePath();
            try {
                Image image = new Image(new FileInputStream(absolutePath));
                ((PhongMaterial) sphere.getMaterial()).setDiffuseMap(image);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static final String RESOURCES_DIR = "/images/";

    private void setPictureOnSphere(Sphere sphere) {
        String fileName = pictureNameToFileNameMap.get(comboBox.getValue());
        Image image = new Image(getClass().getResourceAsStream(RESOURCES_DIR + "input/" + fileName));
        ((PhongMaterial) sphere.getMaterial()).setDiffuseMap(image);
    }


    @SuppressWarnings("SameParameterValue")
    private void fillPictureCombobox(String inputDir) {
        URL url = getClass().getResource(inputDir);
        if (url != null) {
            File styleDirectory = new File(url.getFile());
            if (styleDirectory.isDirectory()) {
                String[] fileNames = styleDirectory.list();
                List.of(Objects.requireNonNull(fileNames)).forEach(this::addFileName);

                List<String> items = comboBox.getItems();
                items.sort(Comparator.naturalOrder());
                comboBox.setValue(!items.isEmpty() ? extractPictureName(items.get(0)) : "");
            }
        } else LOGGER.error(inputDir + " not found...");
    }

    private void addFileName(String fileName) {
        String pictureName = extractPictureName(fileName);
        comboBox.getItems().add(pictureName);
        pictureNameToFileNameMap.put(pictureName, fileName);
    }

    private String extractPictureName(String fileName) {
        String pictureName = fileName.trim()
                .replaceAll("DJI_\\d*", "")
                .replace("_", " ")
                .replace("-", " ")
                .toLowerCase()
                .replace(".jpg", "")
                .replace(".png", "")
                .trim();
        pictureName = pictureName.substring(0, 1).toUpperCase() + pictureName.substring(1).toLowerCase();
        return pictureName;
    }

    private Sphere prepare3DImageViewSphere() {
        PhongMaterial earthMaterial = new PhongMaterial();
        Sphere sphere = new Sphere(150);
        sphere.setMaterial(earthMaterial);
        return sphere;
    }

}
