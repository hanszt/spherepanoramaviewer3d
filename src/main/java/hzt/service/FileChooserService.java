package hzt.service;

import javafx.stage.FileChooser;

import java.io.File;

import static java.lang.System.getProperty;

public class FileChooserService {

   private FileChooserService() {

   }

    public static void configureFileChooser(final FileChooser fileChooser){
        fileChooser.setTitle("View Pictures");
        fileChooser.setInitialDirectory(new File(getProperty("user.home")));
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );
    }

}
